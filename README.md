# Automate Running and Monitoring Application on EC2 instance with Python

#### Project Outline

In this project we will utilise a Python script which automatically creates EC2 instance, install Docker inside and starts Nginx application as Docker container and starts monitoring the application as a scheduled task.

The script will achieve the following

•	Start EC2 instance in default VPC

•	Wait until the EC2 server is fully initialized

•	Install Docker on the EC2 server

•	Start nginx container

•	Open port for nginx to be accessible from browser

•	Create a scheduled function that sends request to the nginx application and checks the status is OK

•	If status is not OK 5 times in a row, it restarts the nginx application

#### Lets get started

The following pre-requisites needs to be done for the script execution

- open the SSH port 22 in the default security group in our default VPC 

- create key-pair for your ec2 instance. Download the private key of the key-pair and set its access permission to 400 mode

- set the values for: image_id, key_name, instance_type and ssh_private_key_path in our python script

Pre-requisite 1

-	open the SSH port 22 in the default security group in your default VPC

Lets navigate to our Frankfurt region

![Image1](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image1.png)

Opening port 22

![Image2](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image2.png)

Pre-requisite 2

-   create key-pair for your ec2 instance. Download the private key of the key-pair and set its access permission to 400 mode

![Image3](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image3.png)

Can then download and set the permissions

![Image4](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image4.png)

Pre-requisite 3

-  set the values for: image_id, key_name, instance_type and ssh_private_key_path in our python script

Have now also set the variables and values

![Image5](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image5.png)

•	Create EC2 instance

![Image6](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image6.png)

Now lets create the ec2 instance, however before we create we need to check if there is an existing instance with the same tag

Using the below link

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/client/describe_instances.html

![Image7](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image7.png)


Now using the below link, we can begin creating the EC2 instance in the script

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/service-resource/create_instances.html#create-instances


Like the below

![Image8](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image8.png)

Now we can implement the logic so that we can see if the instance state is running

•	Wait until the EC2 server is fully initialized

Can also use the below documentation

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/client/describe_instance_status.html

![Image9](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image9.png)


Modifying further, can import the time

![Image11](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image11.png)

![Image12](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image12.png)

Now let’s get the IP of the created ec2 instance

Using the below documentation to contract the below

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/client/describe_instances.html

![Image13](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image13.png)

Now ready to install docker container on it

•	Install Docker on the EC2 server, Start nginx container

Let’s import paramiko to use SSH commands

![Image14](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image14.png)

Can then configure the code which can which achieve the below

Login to the ec2 instance and and install docker

A few things to note

Can also introduce stdin, stdout, stderr

all function exec command this is to execute commands on the cloud server however we want to see the output of this execute command and we get three output of this execute command stdin, stdout and stderr so save this as variables for this command and print stdin and stdout

Also, When we first ssh into the server we get a prompt in which we need to accept by pressing y, because of this we will need the function set missing host key policy and insert auto add policy to the parameter

Can also save the docker commands as a variable

Below is the configured code

![Image15](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image15.png)

Next we need Open port for nginx to be accessible from browser

•	Open port for nginx to be accessible from browser

Despite the opening it up in the console can input a logic which completes it

![Image16](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image16.png)


Now to configure the logic can use the below link

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/client/describe_security_groups.html

Can fetch the existing rule

![Image17](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image17.png)

Can then define the new rule

![Image18](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image18.png)

Can then do comparison of the dictionaries

This code first checks if new_rule is present in existing_rules by comparing dictionaries within existing_rules to new_rule. It uses the any() function to iterate through existing_rules and returns True if any dictionary in existing_rules is equal (contains the same key-value pairs) to new_rule. If rule_exists is False, indicating that new_rule is not found in existing_rules, it appends new_rule to existing_rules using existing_rules.append(new_rule)

![Image19](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image19.png)

Can then use the below link to add the new ingress rule

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/client/authorize_security_group_ingress.html

Can then configure the below

![Image20](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image20.png)

Finalised code for opening the port

![Image21](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image21.png)

Now to achieve the below

•	Create a scheduled function that sends request to the nginx application and checks the status is OK

•	If status is not OK 5 times in a row, it restarts the nginx application

Since we want to make http requests and schedule it, we need to install the packages

https://pypi.org/project/requests/

https://pypi.org/project/schedule/

Can then now import it

![Image23](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image23.png)


Can then configure the counter and do the status checks

![Image24](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image24.png)

Can then configure the below

This code sets up a recurring task using the schedule library. It runs the restart_nginx_if_not_ok function every 30 seconds to check the status of an Nginx application. The while True loop ensures the continuous execution of the scheduled task by checking and running pending tasks every second while allowing a brief delay between iterations

![Image25](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image25.png)

Now running the code


Can see it has printed the public ip

![Image26](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image26.png)

Can see the ec2 instance created

![Image27](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image27.png)

Can also see the docker container got created

![Image28](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image28.png)

Connecting to the ec2 server

```
ssh -i /c/Users/FuadA/Downloads/boto3-server-key.pem ec2-user@3.79.100.106
```

Checking if docker is installed

![Image29](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image29.png)

Checking if nginx was pulled 

![Image30](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image30.png)

As we saw in the console output, the security group is now allowing connection on port 8080

![Image31](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image31.png)

Now let’s test

```
http://3.79.100.106:8080/
```

![Image32](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image32.png)

Now running again

Can see it can see the security group is already added

Can also see after short while the it pings the application successfully

![Image33](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image33.png)

Now let’s stop the container

![Image34](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image34.png)

Can re-run the code and see it has re-started

![Image35](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image35.png)

And nnginx container is now back up

![Image36](https://gitlab.com/FM1995/automate-running-and-monitoring-application-on-ec2-instance-with-python/-/raw/main/Images/Image36.png)



























