import boto3
import time
import paramiko
import requests
import schedule

ec2_client = boto3.client('ec2', region_name='eu-central-1')
ec2_resource = boto3.resource('ec2', region_name='eu-central-1')

# set all needed variable values
image_id = 'ami-0669b163befffbdfc'
key_name = 'boto3-server-key'
instance_type = 't2.micro'
ssh_private_key_path = '/Users/FuadA/Downloads/boto3-server-key.pem'
ssh_user = 'ec2-user'

# check if we have already created this instance using instance name
response = ec2_client.describe_instances(
    Filters=[
        {
            'Name': 'tag:Name',
            'Values': [
                'python-docker-server-project',
            ]
        },
    ],
)

# If no instances are found with the given tag, create a new instance
if len(response['Reservations']) == 0:
    print("No instance found with the given tag. Creating a new instance...")

    # Start EC2 instance
    instance = ec2_resource.create_instances(
        ImageId=image_id,
        InstanceType=instance_type,
        KeyName=key_name,
        MaxCount=1,
        MinCount=1,
        TagSpecifications=[
            {
                'ResourceType': 'instance',
                'Tags': [
                    {
                        'Key': 'Name',
                        'Value': 'python-docker-server-project',
                    },
                ]
            },
        ],
    )

    # Store the instance ID
    instance_id = instance[0].id


    # Get instance state using instance ID
    instance_state = ec2_client.describe_instances(InstanceIds=[instance_id])

    # Check instance status in a loop
    while True:
        instance_state = ec2_client.describe_instances(InstanceIds=[instance_id])

        if instance_state['Reservations'][0]['Instances'][0]['State']['Name'] == 'running':
            print("Instance is fully running now.")
            break # Exit the loop once the instance is running

        print("Instance is not fully running yet. Waiting....")
        time.sleep(5) # Wait for 5 seconds before checking again




else:
    print("Instance with the given tag already exists.")

    # Get instance ID from the existing instance
    ec2_instance = ec2_client.describe_instances(
        InstanceIds=[response['Reservations'][0]['Instances'][0]['InstanceId']],
    )

    instance_id = ec2_instance['Reservations'][0]['Instances'][0]['InstanceId']

# Getting IP of ec2 instance
ec2_instance = ec2_client.describe_instances(
    InstanceIds=[instance_id],
)

ec2_ip = ec2_instance['Reservations'][0]['Instances'][0]['PublicIpAddress']

#Setting EC2 commands
docker_commands = [
    'sudo yum update -y && sudo yum install -y docker',
    'sudo systemctl start docker',
    'sudo usermod -aG docker ec2-user',
    'docker run -d -p 8080:80 --name nginx nginx'
]

#Logging and connecting to into ec2 instance
print('Connecting to ec2 server')
print(f"public ip: {ec2_ip}")
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(hostname=ec2_ip, username=ssh_user, key_filename=ssh_private_key_path)

# Now logging into ec2 server and downloading docker
for command in docker_commands:
    stdin, stdout, stderr = ssh.exec_command(command)
    print(stdout.readlines())

ssh.close()

# Fetch existing inbound rules for the default security group
security_group_id = ec2_client.describe_security_groups(GroupNames=['default'])['SecurityGroups'][0]['GroupId']
existing_rules = ec2_client.describe_security_groups(GroupIds=[security_group_id])['SecurityGroups'][0]['IpPermissions']

# Define the new rule to be added
new_rule = {'IpProtocol': 'tcp', 'FromPort': 8080, 'ToPort': 8080, 'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}

# Check if the new rule already exists in the existing rules
rule_exists = False
for rule in existing_rules:
    if rule['FromPort'] == 8080 and rule['ToPort'] == 8080 and {'CidrIp': '0.0.0.0/0'} in rule['IpRanges']:
        rule_exists = True
        break

if not rule_exists:
    # If the rule doesn't exist, add it to the existing rules
    ec2_client.authorize_security_group_ingress(
        GroupId=security_group_id,
        IpPermissions=[new_rule]
    )
    print("Security group updated to allow incoming traffic on port 8080.")
else:
    print("The rule already exists.")

# Scheduled function to check nginx application status and reload if not OK 5x in a row

# Define a function to check the NGINX status remotely
def check_nginx_status(ec2_ip):
    try:
        response = requests.get(f"http://{ec2_ip}:8080")
        if response.status_code == 200:
            print("Nginx status: OK")
        else:
            print("Nginx status: NOT OK")
            restart_nginx_if_not_ok(ec2_ip)
    except requests.RequestException as e:
        print("Error:", e)
        restart_nginx_if_not_ok(ec2_ip)

def restart_nginx_if_not_ok(ec2_ip):
    counter = 0
    max_retries = 5  # Define the maximum number of retries

    while counter < max_retries:
        try:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(hostname=ec2_ip, username=ssh_user, key_filename=ssh_private_key_path)
            # Restart NGINX container
            ssh.exec_command("docker restart nginx")
            ssh.close()
            print("NGINX container restarted.")
            break  # Exit the loop if the restart was successful
        except paramiko.SSHException as e:
            print("SSH Error:", e)
            counter += 1
            if counter == max_retries:
                print("Maximum retries reached. Could not restart NGINX container.")
            else:
                print(f"Retrying... Attempt {counter}/{max_retries}")
                time.sleep(5)  # Adjust the delay between retries if needed

# Schedule the check_nginx_status function to run every 30 seconds
schedule.every(30).seconds.do(lambda: check_nginx_status(ec2_ip))

# Run the scheduled tasks indefinitely
while True:
    schedule.run_pending()
    time.sleep(1)




















